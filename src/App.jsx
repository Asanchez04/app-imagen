import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar } from './components/Navbar';
import { Footer } from './components/Footer';
import { ListUsers } from './components/ListUser';
import { FotoList } from './components/FotoList';
import { CommentList } from './components/CommentList';
import { Home } from './components/Home'
import { Routes, Route } from "react-router-dom"

function App() {
  return (
    <div className='App'>
      <Navbar />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/Usuarios' element={<ListUsers />} />
        <Route path='/Fotos' element={<FotoList />} />
        <Route path='/Comentarios' element={<CommentList />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
