import { FotoList } from './FotoList';
import { CommentList } from './CommentList';
import { ListUsers } from './ListUser';


export function Home() {
  return (
    <div>
      <ListUsers />
      <FotoList />
      <CommentList />
    </div>
  );
}
