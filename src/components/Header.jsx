import { Navbar } from './Navbar';

export function Header({ Titulo, children }) {
  return (
    <header className='text-center'>
      <h1>{Titulo}</h1>
      {children}
    </header>
  );
}
