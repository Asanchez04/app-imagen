import { User } from './User';
import { Header } from './Header';
import { useState, useEffect } from 'react';

const URL_USUARIOS = 'https://jsonplaceholder.typicode.com/users';

export function ListUsers() {
  let [usuarios, setUsuarios] = useState([]);
  useEffect(() => {
    fetch(URL_USUARIOS)
      .then((res) => res.json())
      .then((datos) => {
        setUsuarios(datos);
      });
  }, []);

  console.log(usuarios);

  return (
    <Header Titulo='Usuarios'>
      <div className='container'>
        <div className='row'>
          {usuarios.map((usuario) => {
            return (
              <div className='col-3 m-3' key={usuario.name}>
                <User
                  {...usuario}
                />
              </div>
            );
          })}
        </div>
      </div>
    </Header>
  );
}
