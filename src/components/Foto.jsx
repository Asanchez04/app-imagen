export function Foto({ title, thumbnailUrl }) {
  return (
    <div>
      <div className='card' style={{ width: '18rem' }}>
        <img src={thumbnailUrl} className='card-img-top' alt={title} />
        <div className='card-body'>
          <p className='card-text'>{title}</p>
        </div>
      </div>
    </div>
  );
}
