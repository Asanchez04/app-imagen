import { Header } from './Header'
import { Link} from 'react-router-dom'
export function Navbar() {
  return (
    <Header Titulo='GALERIA'>
      <div>
        <nav className='navbar navbar-expand-lg navbar-light bg-light'>
          <div className='container-fluid'>
            <button
              className='navbar-toggler'
              type='button'
              data-bs-toggle='collapse'
              data-bs-target='#navbarNav'
              aria-controls='navbarNav'
              aria-expanded='false'
              aria-label='Toggle navigation'
            >
              <span className='navbar-toggler-icon'></span>
            </button>
            <div className='collapse navbar-collapse' id='navbarNav'>

              <ul className='navbar-nav d-flex justify-content-between'>

                <li className='nav-item'>
                  <Link className='nav-link' to='/'> Home</Link>
                </li>
                <li className='nav-item'>
                  <Link className='nav-link' to='/Usuarios'> Usuarios</Link>
                </li>

                <li className='nav-item'>
                  <Link className='nav-link' to='/Fotos'> Fotos</Link>
                </li>

                <li className='nav-item'>
                  <Link className='nav-link' to='/Comentarios'> Comentarios</Link>
                </li>

              </ul>

            </div>
          </div>
        </nav>
      </div>
    </Header>
  );
}
