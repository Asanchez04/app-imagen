export function User({ name, username, email }) {
  return (
    <div>
      <div>
        <div className='card'>
          <h5 className='card-header'>{name}</h5>
          <div className='card-body'>
            <h2>{username}</h2>
            <p className='card-text'>{email}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
