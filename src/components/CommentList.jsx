import { Comment } from './Comment';
import { Header } from './Header';
import { useState, useEffect } from 'react';

const URL_COMENTARIOS = 'https://jsonplaceholder.typicode.com/comments';
export function CommentList() {
  let [comments, setComments] = useState([]);

  useEffect(() => {
    fetch(URL_COMENTARIOS)
      .then((res) => res.json())
      .then((data) => {
        setComments(data);
      });
  }, []);

  console.log(comments);

  return (
    <Header Titulo='Comentarios'>
      <div className='container'>
        <div className='row'>
          {comments.slice(0, 24).map((comment) => {
            return (
              <div className='col-3 m-3' key={comment.name}>
                <Comment
                  {...comment}
                />
              </div>
            );
          })}
        </div>
      </div>
    </Header>
  );
}
