import { Foto } from './Foto';
import { Header } from './Header';
import { useState, useEffect } from 'react';

const URL_FOTOS = 'https://jsonplaceholder.typicode.com/photos';

export function FotoList() {
  let [fotos, setFotos] = useState([]);
  useEffect(() => {
    fetch(URL_FOTOS)
      .then((res) => res.json())
      .then((datos) => {
        setFotos(datos);
      });
  }, []);

  return (
    <Header Titulo='Fotos'>
      <div className='container'>
        <div className='row'>
          {fotos.slice(0, 24).map((foto) => {
            return (
              <div className='col-3 m-4' key={foto.id}>
                <Foto title={foto.title} thumbnailUrl={foto.thumbnailUrl} />
              </div>
            );
          })}
        </div>
      </div>
    </Header>
  );
}
